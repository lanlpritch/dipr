
DIPR_VERSION = "V1.0.0"
DIPR_RELEASE_DATE = "04-09-2018"
DIPR_URL = "https://dipr.dev"


def get_version_string():
    return "Version: " + DIPR_VERSION + " (" + DIPR_RELEASE_DATE + ")"
