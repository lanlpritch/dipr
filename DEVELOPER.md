
Developer Instructions
======================
Development and building of binaries for dipr requires a correctly configured Anaconda3 environment.  An environment
configuration is provided in the "Environment" directory.  To create a dipr environment, open a conda prompt and:

    conda env create -f environment-common.yaml
    
Then

    conda activate diprenv
    
Building Instructions
=====================
Create and activate the conda environment as specified above.  Change to the "Build" directory and run:

    pyinstaller --onefile dipr.spec
    
If all goes well, a binary for the current platform should be generated in the "Build/dist" folder. 

    